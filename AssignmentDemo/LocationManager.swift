//
//  LocationManager.swift
//  AssignmentDemo
//
//  Created by SoluLab on 11/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import Foundation
import CoreLocation

class LocationManager: NSObject, CLLocationManagerDelegate {

    //Variable declaration
    var locationManager = CLLocationManager()
    var currentLocation: CLLocation?
    var arrLocationDetail = [String]()
    var currentLatLng = [String]()
    var timeStempArr = [String]()
    var notificatoinNumber: Int = 0
    var totalDistance: Int = 0
    var isCurrentLatLong: Bool = false
    var fstlat: Double = 0
    var fstlong: Double = 0
    
    public class var sharedInstance: LocationManager {
        struct Singleton {
            static let instance: LocationManager = LocationManager()
        }
        return Singleton.instance
    }

    override init() {
        super.init()
        if let timeStempArr = UserDefaults.standard.object(forKey: "timeStempArr") as? [String] {
            self.timeStempArr = timeStempArr
        }
        self.enableLocationService()
    }

    func enableLocationService() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
    }

    //Location manager delegate method

    func locationManager(_ manager:CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last
        if isCurrentLatLong == false{
            fstlat = (currentLocation?.coordinate.latitude)!
            fstlong = (currentLocation?.coordinate.longitude)!
            isCurrentLatLong = true
        }

        let coordinate0 = CLLocation(latitude: fstlat, longitude: fstlong)
        let coordinate1 = CLLocation(latitude: (currentLocation?.coordinate.latitude)!, longitude: (currentLocation?.coordinate.longitude)!)
        let distanceInMeters = coordinate0.distance(from: coordinate1)
        if distanceInMeters >= 50 {
            NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "loadDataDistace"), object: nil)
            fstlat = (currentLocation?.coordinate.latitude)!
            fstlong = (currentLocation?.coordinate.longitude)!
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateTabBadge"), object: nil)
            self.saveTimestamp()
        }

        totalDistance = totalDistance + Int(distanceInMeters)

        NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "loadData"), object: nil)
        let meter = String(format: "%.5f", (distanceInMeters))
        arrLocationDetail.append("\(totalDistance) Total meter")
        currentLatLng.append("\(meter) meter")
    }

    func saveTimestamp() {
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = "EEEE, dd-MM-yyyy, hh:mm"
        let date = dateFormatter1.string(from: NSDate() as Date)
        timeStempArr.append("\(date)")
        UserDefaults.standard.set(timeStempArr, forKey: "timeStempArr")
    }

}
