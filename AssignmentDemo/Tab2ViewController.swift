//
//  Tab2ViewController.swift
//  AssignmentDemo
//
//  Created by SoluLab on 06/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

class Tab2ViewController: UIViewController {

    //Variable declaration
    @IBOutlet var tblObj: UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self.tblObj!, selector: #selector(self.tblObj?.reloadData), name: NSNotification.Name(rawValue: "loadData"), object: nil)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblObj?.reloadData()
        tabBarController?.tabBar.items![1].badgeValue = nil
        LocationManager.sharedInstance.notificatoinNumber = 0
    }

}

extension Tab2ViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LocationManager.sharedInstance.arrLocationDetail.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.detailTextLabel?.text = "\(LocationManager.sharedInstance.arrLocationDetail[indexPath.row])"
        cell?.textLabel?.text = "\(LocationManager.sharedInstance.currentLatLng[indexPath.row])"
        return cell!
    }
}
