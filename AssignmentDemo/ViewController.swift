//
//  ViewController.swift
//  AssignmentDemo
//
//  Created by SoluLab on 06/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class ViewController: UIViewController,CLLocationManagerDelegate,MKMapViewDelegate {

    // Variable declaration
    @IBOutlet weak var mapObj: MKMapView!
    var cityName: String? = ""
    var address: String? = ""

    //MARK:- View lifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        LocationManager.sharedInstance.enableLocationService()
        cityName = ""
        address = ""
        Timer.scheduledTimer(timeInterval: TimeInterval(5), target: self, selector: #selector(self.updateAnnoantion), userInfo: nil, repeats: true)

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateTabBadge), name: NSNotification.Name(rawValue: "updateTabBadge"), object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let locationTmp = LocationManager.sharedInstance.currentLocation {
            let annotationPoint: MKMapPoint  = MKMapPointForCoordinate(locationTmp.coordinate);
            let zoomRect: MKMapRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.5, 0.5);
            mapObj.setVisibleMapRect(zoomRect, animated: true)
        }
    }

    //MARK:- Helper Methods
    func updateTabBadge() {
        if self.tabBarController?.selectedIndex != 1 {
            LocationManager.sharedInstance.notificatoinNumber = LocationManager.sharedInstance.notificatoinNumber + 1
            tabBarController?.tabBar.items![1].badgeValue = "\(LocationManager.sharedInstance.notificatoinNumber)"
        }
    }

    func updateAnnoantion() {
        if let locationTmp = LocationManager.sharedInstance.currentLocation {

            let location = CLLocation(latitude: (locationTmp.coordinate.latitude), longitude: (locationTmp.coordinate.longitude)) //changed!!!

            let geoCoder = CLGeocoder()

            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in

                // Place details
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]

                if placeMark == nil {
                    return
                }

                // Address dictionary
                var strAddress: String?

                if let addressDict = placeMark.addressDictionary as? [String: AnyObject] {

                    // City
                    if let city = addressDict["City"] as? String {
                        self.address = (city as String) as String
                    }

                    // state
                    if let state = addressDict["State"]  as? String {
                        strAddress = state
                    }
                    
                    // Country
                    if let country = addressDict["Country"]  as? String {
                        strAddress = strAddress! + " " + country
                    }

                    let annon = Annotation.init()
                    annon.title = self.address
                    annon.subtitle = strAddress
                    annon.coordinate = (locationTmp.coordinate)

                    if self.mapObj.annotations.count > 0 {
                        self.mapObj.removeAnnotation(self.mapObj.annotations.first!)
                    }
                    self.mapObj.addAnnotation(annon)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
