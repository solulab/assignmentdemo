//
//  Annotation.swift
//  AssignmentDemo
//
//  Created by SoluLab on 11/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class Annotation: NSObject, MKAnnotation {

    var coordinate = CLLocationCoordinate2D()
    var title: String?
    var subtitle: String?

    override init() {
        super.init()
    }
}
