//
//  AssignmentDemoTests.swift
//  AssignmentDemoTests
//
//  Created by SoluLab on 11/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import XCTest
@testable import AssignmentDemo

class AssignmentDemoTests: XCTestCase {

    var tab1: ViewController!
    var tab2: Tab2ViewController!
    var tab3: Tab3ViewController!

    override func setUp() {
        super.setUp()
        let storyboard = UIStoryboard(name: "Main",
                                      bundle: Bundle.main)
        let tabbarViewController = storyboard.instantiateInitialViewController() as! UITabBarController

        tab1 = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController

        let navi1 = UINavigationController.init(rootViewController: tab1)

        tab2 = storyboard.instantiateViewController(withIdentifier: "Tab2ViewController") as! Tab2ViewController

        let navi2 = UINavigationController.init(rootViewController: tab2)

        tab3 = storyboard.instantiateViewController(withIdentifier: "Tab3ViewController") as! Tab3ViewController
        let navi3 = UINavigationController.init(rootViewController: tab3)

        tabbarViewController.viewControllers = [navi1,navi2,navi3]
        UIApplication.shared.keyWindow!.rootViewController = tabbarViewController
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
