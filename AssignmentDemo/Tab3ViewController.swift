//
//  Tab3ViewController.swift
//  AssignmentDemo
//
//  Created by SoluLab on 06/01/17.
//  Copyright © 2017 SoluLab. All rights reserved.
//

import UIKit

class Tab3ViewController: UIViewController {

    @IBOutlet var tblObj: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self.tblObj!, selector: #selector(self.tblObj?.reloadData), name: NSNotification.Name(rawValue: "loadDataDistace"), object: nil)

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tblObj?.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension Tab3ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return LocationManager.sharedInstance.timeStempArr.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
        cell?.textLabel?.text = "\(LocationManager.sharedInstance.timeStempArr[indexPath.row])"
        return cell!

    }

}
